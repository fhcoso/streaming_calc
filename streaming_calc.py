#!/usr/bin/env python3
# coding: utf-8
# © 2016 Florent H. CARRÉ (COLUNDRUM)

"""Bandwidth calculation for streaming server."""

import argparse
import sys


def bw_server(args_bw_server):
    """Determine necessary server bandwidth."""
    sum_bw_server = \
        args_bw_server.nblisteners * args_bw_server.bitrate * 1000 / 1024
    print(
        'Number of listeners : {}\n'
        'Bitrate (kb/s) : {}\n'
        'Server bandwidth (Mib/s) : {}'
        .format(
            args_bw_server.nblisteners,
            args_bw_server.bitrate,
            sum_bw_server
        )
    )


def server_usage_bw(args_server_usage_bw):
    """Determine the amount of data used for the streaming."""
    sum_server_usage_bw = \
        args_server_usage_bw.nbdays * args_server_usage_bw.nbhours * \
        3600 * args_server_usage_bw.bitrate * 1000 / 8 * \
        args_server_usage_bw.nblisteners / 1024 / 1024
    print(
        'Number of listeners : {}\n'
        'Bitrate (kb/s) : {}\n'
        'Number of days : {}\n'
        'Number of hours by days : {}\n'
        'Bandwidth used (GiB) : {}'
        .format(
            args_server_usage_bw.nblisteners,
            args_server_usage_bw.bitrate,
            args_server_usage_bw.nbdays,
            args_server_usage_bw.nbhours,
            sum_server_usage_bw
        )
    )


def main():
    """Main function with parsing arguments."""
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    parser_bwserver = subparsers.add_parser(
        'bwserver',
        help='Determine necessary server bandwidth'
    )
    parser_bwserver.add_argument(
        'nblisteners',
        type=int,
        help='number of listeners'
    )
    parser_bwserver.add_argument(
        'bitrate',
        type=int,
        help='bitrate in kb/s'
    )
    parser_bwserver.set_defaults(func=bw_server)

    parser_serverusagebw = subparsers.add_parser(
        'usagebw',
        help='Determine the amount of data used for the streaming'
    )
    parser_serverusagebw.add_argument(
        'nblisteners',
        type=int,
        help='number of listeners')
    parser_serverusagebw.add_argument(
        'bitrate',
        type=int,
        help='bitrate in kb/s')
    parser_serverusagebw.add_argument(
        'nbdays',
        type=int,
        help='number of days')
    parser_serverusagebw.add_argument(
        'nbhours',
        type=int,
        help='number of hours by days (integer only)')
    parser_serverusagebw.set_defaults(func=server_usage_bw)

    args = parser.parse_args()
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    args.func(args)


if __name__ == '__main__':
    main()
